package dk.itu.moapd.bikeshare.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import dk.itu.moapd.bikeshare.entities.Bike
import dk.itu.moapd.bikeshare.entities.Ride

@Database(entities = [ Bike::class, Ride::class ], version=15, exportSchema = false)
@TypeConverters(BikeTypeConverters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun bikeDao(): BikeDao
    abstract fun rideDao(): RideDao
}