package dk.itu.moapd.bikeshare.database

import androidx.lifecycle.LiveData
import androidx.room.*
import dk.itu.moapd.bikeshare.entities.Ride
import java.util.*

@Dao
interface RideDao {

    @Query("SELECT * FROM ride")
    fun getRides(): LiveData<List<Ride>>

    @Query("SELECT * FROM ride WHERE price = 0")
    fun getActiveRides(): LiveData<List<Ride>>

    @Query("SELECT * FROM ride WHERE rideId=(:rideId)")
    fun getRide(rideId: UUID): LiveData<Ride?>

    @Update
    fun updateRide(ride: Ride)

    @Insert
    fun addRide(ride: Ride)

    @Delete
    fun deleteRide(ride: Ride)
}