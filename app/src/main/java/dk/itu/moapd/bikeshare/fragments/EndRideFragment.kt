package dk.itu.moapd.bikeshare.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dk.itu.moapd.bikeshare.R
import dk.itu.moapd.bikeshare.entities.Ride
import dk.itu.moapd.bikeshare.utilities.checkPermissions
import dk.itu.moapd.bikeshare.utilities.getDate
import dk.itu.moapd.bikeshare.utilities.isLocationEnabled
import dk.itu.moapd.bikeshare.utilities.requestLocationPermissions
import dk.itu.moapd.bikeshare.viewModels.BikeVM


class EndRideFragment: Fragment() {

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var rideRecyclerView: RecyclerView
    private lateinit var adapter: EndRideAdapter

    private val bikeVM: BikeVM by lazy {
        ViewModelProviders.of(this).get(BikeVM::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_end_ride, container, false)
        rideRecyclerView = view.findViewById(R.id.ride_recycler_view) as RecyclerView
        rideRecyclerView.layoutManager = LinearLayoutManager(context)
        rideRecyclerView.adapter = EndRideAdapter(emptyList())

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.end_ride)
        bikeVM.activeRideListLiveData.observe( // get list of bikes and put into list
            viewLifecycleOwner,
            Observer { rides ->
                rides?.let {
                    updateUI(rides)
                }
            })
    }

    private fun updateUI(rides: List<Ride>) {
        adapter = EndRideAdapter(rides)
        rideRecyclerView.adapter = adapter
    }

    private inner class EndRideAdapter(private var rides: List<Ride>): RecyclerView.Adapter<EndRideHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EndRideHolder {
            val view = layoutInflater.inflate(R.layout.recyclerview_end_ride, parent, false)
            return EndRideHolder(view)
        }

        override fun getItemCount() = rides.size

        override fun onBindViewHolder(holder: EndRideHolder, position: Int) {
            val ride = rides[position]
            holder.bind(ride)
        }
    }

    private inner class EndRideHolder(view: View) : RecyclerView.ViewHolder(view) {

        private lateinit var ride: Ride
        private val startTimeTextView: TextView = itemView.findViewById(R.id.ride_start_time)
        private val stopButton: Button = itemView.findViewById(R.id.endButton)

        fun bind(ride: Ride) {
            this.ride = ride
            startTimeTextView.text = getDate(ride.startTime).toString()
            stopButton.setOnClickListener {
                bikeVM.getBike(ride.relatedBikeId).observe(
                    viewLifecycleOwner,
                    Observer {
                        if (it!!.active) { // check for permissions
                            if (!checkPermissions(activity!!)) {
                                requestLocationPermissions(activity!!)
                            } else if (!isLocationEnabled(activity!!)) {
                                Toast.makeText(activity, R.string.turn_on_gps, Toast.LENGTH_LONG).show()
                            } else {
                                val bikePrice = it.price
                                val currentTime = System.currentTimeMillis()
                                val rideTime = currentTime - ride.startTime
                                val price = (((rideTime / ((1000 * 60 * 60)) % 24) * bikePrice) + bikePrice).toInt()
                                val builder = AlertDialog.Builder(activity!!)
                                val message = StringBuilder()
                                    .append(getString(R.string.end_would_you))
                                    .append("\n")
                                    .append(getString(R.string.end_price)).append(" $price ").append(getString(R.string.currency))
                                builder.setTitle(getString(R.string.end_end_ride))
                                builder.setMessage(message)
                                builder.setPositiveButton(android.R.string.yes) { _, _ ->
                                    fusedLocationProviderClient.lastLocation.addOnSuccessListener { location ->
                                        // ending the ride and updating the bike
                                        ride.endTime = currentTime
                                        ride.endLat = location.latitude
                                        ride.endLng = location.longitude
                                        ride.price = price
                                        bikeVM.saveRide(ride)
                                        it.active = false
                                        it.lat = location.latitude
                                        it.lng = location.longitude
                                        bikeVM.saveBike(it)
                                    }
                                }
                                builder.setNegativeButton(android.R.string.no) { _, _ -> }
                                builder.show()
                            }
                        }
                    }
                )
            }
        }
    }
}
