package dk.itu.moapd.bikeshare.fragments
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import dk.itu.moapd.bikeshare.R
import dk.itu.moapd.bikeshare.utilities.checkPermissions
import dk.itu.moapd.bikeshare.utilities.isLocationEnabled
import dk.itu.moapd.bikeshare.utilities.requestLocationPermissions

// The main window, used for navigation buttons
class MainFragment : Fragment() {

    private lateinit var mButtonStartRide: Button
    private lateinit var mButtonEndRide: Button
    private lateinit var mButtonRegisterBike: Button
    private lateinit var mButtonCheckBike: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)
        mButtonStartRide = view.findViewById(R.id.startRide)
        mButtonEndRide = view.findViewById(R.id.endRide)
        mButtonRegisterBike = view.findViewById(R.id.registerBike)
        mButtonCheckBike = view.findViewById(R.id.checkBike)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.app_name)
    }

    override fun onStart() {
        super.onStart()

        mButtonStartRide.setOnClickListener{
            if(!checkPermissions(activity!!)) {
                requestLocationPermissions(activity!!)
            } else if (!isLocationEnabled(activity!!)) {
                Toast.makeText(context, R.string.turn_on_gps, Toast.LENGTH_LONG).show()
            } else {
                val nextFrag = StartRideFragment()
                activity!!.supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment, nextFrag)
                    .addToBackStack(null)
                    .commit()
            }
        }
        mButtonEndRide.setOnClickListener{
            val nextFrag = EndRideFragment()
            activity!!.supportFragmentManager.beginTransaction()
                .replace(R.id.fragment, nextFrag)
                .addToBackStack(null)
                .commit()
        }
        mButtonRegisterBike.setOnClickListener{
            val nextFrag =
                RegisterBikeFragment()
            activity!!.supportFragmentManager.beginTransaction()
                .replace(R.id.fragment, nextFrag)
                .addToBackStack(null)
                .commit()
        }
        mButtonCheckBike.setOnClickListener{
            val nextFrag = CheckBikeFragment()
            activity!!.supportFragmentManager.beginTransaction()
                .replace(R.id.fragment, nextFrag)
                .addToBackStack(null)
                .commit()
        }
    }

}
