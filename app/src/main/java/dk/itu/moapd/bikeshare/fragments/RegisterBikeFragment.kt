package dk.itu.moapd.bikeshare.fragments

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import dk.itu.moapd.bikeshare.R
import dk.itu.moapd.bikeshare.entities.Bike
import dk.itu.moapd.bikeshare.entities.Type
import dk.itu.moapd.bikeshare.utilities.checkPermissions
import dk.itu.moapd.bikeshare.utilities.getScaledBitmap
import dk.itu.moapd.bikeshare.utilities.isLocationEnabled
import dk.itu.moapd.bikeshare.utilities.requestLocationPermissions
import dk.itu.moapd.bikeshare.viewModels.RegisterBikeVM
import java.io.File

private const val REQUEST_PHOTO = 2

class RegisterBikeFragment : Fragment() {
    private lateinit var mSpinnerBikeType: Spinner
    private lateinit var mEditTextBikePrice: EditText
    private lateinit var mButtonTakePicture: Button
    private lateinit var mButtonRegister: Button
    private lateinit var mPhotoView: ImageView

    private lateinit var bike: Bike
    private lateinit var photoFile: File
    private lateinit var photoUri: Uri
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private val registerBikeVM: RegisterBikeVM by lazy {
        ViewModelProviders.of(this).get(RegisterBikeVM::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null) {
            val json = savedInstanceState.getString("Bike")
            val gson = Gson()
            this.bike = gson.fromJson(json, Bike::class.java)
        } else {
            this.bike = Bike()
        }
        registerBikeVM.loadBike(bike)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_register_bike, container, false)
        mSpinnerBikeType = view.findViewById(R.id.spinnerBikeType)
        mEditTextBikePrice = view.findViewById(R.id.editTextBikePrice)
        mButtonTakePicture = view.findViewById(R.id.TakePicture)
        mButtonRegister = view.findViewById(R.id.Register)
        mPhotoView = view.findViewById(R.id.bike_photo) as ImageView

        val adapter = ArrayAdapter(
                                            context!!,
                                            android.R.layout.simple_spinner_item,
                                            resources.getStringArray(R.array.bike_types))
        mSpinnerBikeType.adapter = adapter

        return view
    }

    private fun updateUI() {
        updatePhotoView()
    }

    private fun updatePhotoView() {
        if (photoFile.exists()) {
            val bitmap = getScaledBitmap(
                photoFile.path,
                this.activity!!
            )
            mPhotoView.setImageBitmap(bitmap)
        } else {
            mPhotoView.setImageResource(R.drawable.bike)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState != null) {
            mSpinnerBikeType.setSelection(savedInstanceState.getInt("BikeType"))
            mEditTextBikePrice.setText(savedInstanceState.getString("BikePrice"))
        }
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.register_bike)
        registerBikeVM.bikeLiveData.observe(
            viewLifecycleOwner,
            Observer { bike ->
                bike?.let {
                    this.bike = bike
                    photoFile = registerBikeVM.getPhotoFile()
                    photoUri = FileProvider.getUriForFile(requireActivity(),
                        "dk.itu.moapd.bikeshare.fileprovider",
                        photoFile)
                    updateUI()
                }
            }
        )
    }

    override fun onStart() {
        super.onStart()

        mButtonRegister.setOnClickListener{
            when {
                mEditTextBikePrice.text.isEmpty() -> {
                    Toast.makeText(context,
                        R.string.enter_bike_price,Toast.LENGTH_SHORT).show()
                }
                mEditTextBikePrice.text.toString().trim().toInt() == 0 -> {
                    Toast.makeText(context,
                        R.string.entered_wrong_price,Toast.LENGTH_SHORT).show()
                }
                !photoFile.exists() -> {
                    Toast.makeText(context,
                        R.string.no_picture,Toast.LENGTH_SHORT).show()
                }
                !checkPermissions(activity!!) -> requestLocationPermissions(activity!!)
                !isLocationEnabled(activity!!) -> Toast.makeText(context, R.string.turn_on_gps, Toast.LENGTH_LONG).show()
                else -> {
                    fusedLocationProviderClient.lastLocation.addOnSuccessListener { location ->
                        bike.type = Type(mSpinnerBikeType.selectedItemPosition)
                        bike.price = mEditTextBikePrice.text.toString().toInt()
                        bike.lat = location.latitude
                        bike.lng = location.longitude
                        registerBikeVM.addBike(bike)
                        activity!!.supportFragmentManager.popBackStack()
                    }
                }
            }
        }

        mButtonTakePicture.apply {
            val packageManager: PackageManager = requireActivity().packageManager
            val captureImage = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val resolvedActivity: ResolveInfo? =
                packageManager.resolveActivity(captureImage,
                    PackageManager.MATCH_DEFAULT_ONLY)
            if (resolvedActivity == null) {
                isEnabled = false
            }

            setOnClickListener {
                captureImage.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)

                val cameraActivities: List<ResolveInfo> =
                    packageManager.queryIntentActivities(captureImage,
                        PackageManager.MATCH_DEFAULT_ONLY)

                for (cameraActivity in cameraActivities) {
                    requireActivity().grantUriPermission(
                        cameraActivity.activityInfo.packageName,
                        photoUri,
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                }
                startActivityForResult(captureImage, REQUEST_PHOTO)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when {
            resultCode != Activity.RESULT_OK -> return

            requestCode == REQUEST_PHOTO -> {
                requireActivity().revokeUriPermission(photoUri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                updatePhotoView()
            }
        }
    }

    override fun onDetach() {
        super.onDetach()
        requireActivity().revokeUriPermission(photoUri,
            Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt("BikeType", mSpinnerBikeType.selectedItemPosition)
        outState.putString("BikePrice", mEditTextBikePrice.text.toString())
        val gson = Gson()
        val bikeAsJson: String = gson.toJson(bike)
        outState.putString("Bike", bikeAsJson)
        super.onSaveInstanceState(outState)
    }
}

