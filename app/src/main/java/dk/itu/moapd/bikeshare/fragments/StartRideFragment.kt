package dk.itu.moapd.bikeshare.fragments

import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import dk.itu.moapd.bikeshare.R
import dk.itu.moapd.bikeshare.viewModels.BikeVM
import dk.itu.moapd.bikeshare.entities.Bike
import dk.itu.moapd.bikeshare.entities.Ride
import dk.itu.moapd.bikeshare.utilities.*


class StartRideFragment : Fragment(), OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnInfoWindowCloseListener {

    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val options = MarkerOptions()
    private val bikeVM: BikeVM by lazy {
        ViewModelProviders.of(this).get(BikeVM::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_start_ride, container, false)
        val startRideFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        startRideFragment.getMapAsync(this)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.start_ride)
        bikeVM.bikeListLiveData.observe( // Make all the markers for the map
            viewLifecycleOwner,
            Observer { bikes ->
                bikes?.let {
                    bikes.listIterator().forEach {
                        if(!it.active) {
                            options.position(LatLng(it.lat, it.lng))
                            options.title("Info")
                            mMap.addMarker(options).tag = it
                            mMap.setOnMarkerClickListener(this)
                            mMap.setOnInfoWindowClickListener(this)
                            mMap.setOnInfoWindowCloseListener(this)
                        }
                    }
                }
            }
        )
    }

    fun moveMap (location: Location) {
        mMap.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    location.latitude,
                    location.longitude
                ), 17f
            )
        )
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isCompassEnabled = false
        mMap.uiSettings.isRotateGesturesEnabled = false
        val customInfoWindow = CustomInfoWindowGoogleMap(activity!!)
        mMap.setInfoWindowAdapter(customInfoWindow)

        when {
            !checkPermissions(activity!!) -> requestLocationPermissions(activity!!)
            !isLocationEnabled(activity!!) -> Toast.makeText(context, R.string.turn_on_gps, Toast.LENGTH_LONG).show()
            else -> {
                fusedLocationProviderClient.lastLocation.addOnSuccessListener { location ->
                    if (location != null) {
                        moveMap(location)
                    }
                    moveMap(location)
                }
            }
        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        if (marker.isInfoWindowShown) { // Toggle info window and colour on marker when clicked
            marker.hideInfoWindow()
        } else {
            marker.showInfoWindow()
            marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
        }
        return false
    }

    override fun onInfoWindowClick(marker: Marker) {
        val mBike: Bike = marker.tag as Bike // Popup for renting the bike
        val builder = AlertDialog.Builder(context!!)
        val message = StringBuilder()
            .append(getString(R.string.would_you))
            .append(" ")
            .append(mBike.type.toString())
            .append(" ")
            .append(getString(R.string.for_))
            .append(" ")
            .append(mBike.price.toString())
            .append(" ")
            .append(getString(R.string.an_hour))
            .toString()
        builder.setTitle(R.string.rent_bike)
        builder.setMessage(message)

        builder.setPositiveButton(android.R.string.yes) { _, _ -> // yes -> Start the a ride
            activity!!.supportFragmentManager.popBackStack()
            mBike.active = true
            bikeVM.saveBike(mBike)
            bikeVM.addRide(
                Ride(
                    relatedBikeId = mBike.bikeId,
                    startLat = mBike.lat,
                    startLng = mBike.lng,
                    startTime = System.currentTimeMillis()
                )
            )
            Toast.makeText(context, getString(R.string.ride_started), Toast.LENGTH_SHORT).show()
        }
        builder.setNegativeButton(android.R.string.no) { _, _ -> // no -> close window
            marker.hideInfoWindow()
        }
        builder.show()
    }

    override fun onInfoWindowClose(marker : Marker) {
        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
    }
}
