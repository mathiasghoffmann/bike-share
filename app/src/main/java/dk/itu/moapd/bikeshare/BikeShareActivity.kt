package dk.itu.moapd.bikeshare

import android.app.Activity
import android.os.Bundle
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.*
import dk.itu.moapd.bikeshare.fragments.MainFragment
import dk.itu.moapd.bikeshare.utilities.updateDeviceLocation

// single activity controlling fragments for navigation
class BikeShareActivity : AppCompatActivity() {
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private val locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val locationList = locationResult.locations
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        updateDeviceLocation(this, locationCallback)
        setContentView(R.layout.activity_bike_share)
        val currentFragment =
            supportFragmentManager.findFragmentById(R.id.fragment)

        if (currentFragment == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment,
                    MainFragment()
                )
                .commit()
        }
    }
}